package jose

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"testing"
)

func TestRFC7516_appendix_A_1(t *testing.T) {
	const msg = "The true sign of intelligence is not knowledge but imagination."
	if bytes.Compare([]byte(msg), []byte{
		84, 104, 101, 32, 116, 114, 117, 101, 32, 115, 105, 103, 110, 32,
		111, 102, 32, 105, 110, 116, 101, 108, 108, 105, 103, 101, 110, 99,
		101, 32, 105, 115, 32, 110, 111, 116, 32, 107, 110, 111, 119, 108,
		101, 100, 103, 101, 32, 98, 117, 116, 32, 105, 109, 97, 103, 105,
		110, 97, 116, 105, 111, 110, 46}) != 0 {
		t.Fatal("A.1. Message byte conversion not spec compatible")
	}

	// Content Encryption Key used in the example
	var cek = []byte{177, 161, 244, 128, 84, 143, 225, 115, 63, 180, 3, 255, 107, 154, 212, 246, 138, 7, 110, 91, 112, 46, 34, 105, 47, 130, 203, 46, 122, 234, 64, 252}
	t.Log("Using AES key length of", len(cek))

	// Generate recipient key
	recipient1PrivateKey := &AsymmetricKey{}
	recipient1PrivateKey.keyType = KeyTypeRSA
	recipient1PrivateKey.use = `enc`
	recipient1PrivateKey.keyOperations = []KeyOperation{KeyOperationEncrypt, KeyOperationDecrypt, KeyOperationWrapKey}
	recipient1PrivateKey.n = `oahUIoWw0K0usKNuOR6H4wkf4oBUXHTxRvgb48E-BVvxkeDNjbC4he8rUWcJoZmds2h7M70imEVhRU5djINXtqllXI4DFqcI1DgjT9LewND8MW2Krf3Spsk_ZkoFnilakGygTwpZ3uesH-PFABNIUYpOiN15dsQRkgr0vEhxN92i2asbOenSZeyaxziK72UwxrrKoExv6kc5twXTq4h-QChLOln0_mtUZwfsRaMStPs6mS6XrgxnxbWhojf663tuEQueGC-FCMfra36C9knDFGzKsNa7LZK2djYgyD3JR_MB_4NUJW_TqOQtwHYbxevoJArm-L5StowjzGy-_bq6Gw`
	recipient1PrivateKey.e = `AQAB`
	recipient1PrivateKey.d = `kLdtIj6GbDks_ApCSTYQtelcNttlKiOyPzMrXHeI-yk1F7-kpDxY4-WY5NWV5KntaEeXS1j82E375xxhWMHXyvjYecPT9fpwR_M9gV8n9Hrh2anTpTD93Dt62ypW3yDsJzBnTnrYu1iwWRgBKrEYY46qAZIrA2xAwnm2X7uGR1hghkqDp0Vqj3kbSCz1XyfCs6_LehBwtxHIyh8Ripy40p24moOAbgxVw3rxT_vlt3UVe4WO3JkJOzlpUf-KTVI2Ptgm-dARxTEtE-id-4OJr0h-K-VFs3VSndVTIznSxfyrj8ILL6MG_Uv8YAu7VILSB3lOW085-4qE3DzgrTjgyQ`
	recipient1PrivateKey.p = `1r52Xk46c-LsfB5P442p7atdPUrxQSy4mti_tZI3Mgf2EuFVbUoDBvaRQ-SWxkbkmoEzL7JXroSBjSrK3YIQgYdMgyAEPTPjXv_hI2_1eTSPVZfzL0lffNn03IXqWF5MDFuoUYE0hzb2vhrlN_rKrbfDIwUbTrjjgieRbwC6Cl0`
	recipient1PrivateKey.q = `wLb35x7hmQWZsWJmB_vle87ihgZ19S8lBEROLIsZG4ayZVe9Hi9gDVCOBmUDdaDYVTSNx_8Fyw1YYa9XGrGnDew00J28cRUoeBB_jKI1oma0Orv1T9aXIWxKwd4gvxFImOWr3QRL9KEBRzk2RatUBnmDZJTIAfwTs0g68UZHvtc`
	recipient1PrivateKey.dp = `ZK-YwE7diUh0qR1tR7w8WHtolDx3MZ_OTowiFvgfeQ3SiresXjm9gZ5KLhMXvo-uz-KUJWDxS5pFQ_M0evdo1dKiRTjVw_x4NyqyXPM5nULPkcpU827rnpZzAJKpdhWAgqrXGKAECQH0Xt4taznjnd_zVpAmZZq60WPMBMfKcuE`
	recipient1PrivateKey.dq = `Dq0gfgJ1DdFGXiLvQEZnuKEN0UUmsJBxkjydc3j4ZYdBiMRAy86x0vHCjywcMlYYg4yoC4YZa9hNVcsjqA3FeiL19rk8g6Qn29Tt0cj8qqyFpz9vNDBUfCAiJVeESOjJDZPYHdHY8v1b-o-Z2X5tvLx-TCekf7oxyeKDUqKWjis`
	recipient1PrivateKey.qinv = `VIMpMYbPf47dT1w_zDUXfPimsSegnMOA1zTaX7aGk_8urY6R8-ZW1FxU7AlWAyLWybqq6t16VFd7hQd0y6flUK4SlOydB61gwanOsXGOAOv82cHq0E3eL4HrtZkUuKvnPrMnsUUFlfUdybVzxyjz9JF_XyaY14ardLSjf4L_FNY`

	// Spec test
	/*recipient1PrivateKeyJSON, err := recipient1PrivateKey.MarshalJSON()
	if err != nil {
		t.Fatal(err)
	}
	if bytes.Compare(recipient1PrivateKeyJSON, []byte{
		56, 163, 154, 192, 58, 53, 222, 4, 105, 218, 136, 218, 29, 94, 203,
		22, 150, 92, 129, 94, 211, 232, 53, 89, 41, 60, 138, 56, 196, 216,
		82, 98, 168, 76, 37, 73, 70, 7, 36, 8, 191, 100, 136, 196, 244, 220,
		145, 158, 138, 155, 4, 117, 141, 230, 199, 247, 173, 45, 182, 214,
		74, 177, 107, 211, 153, 11, 205, 196, 171, 226, 162, 128, 171, 182,
		13, 237, 239, 99, 193, 4, 91, 219, 121, 223, 107, 167, 61, 119, 228,
		173, 156, 137, 134, 200, 80, 219, 74, 253, 56, 185, 91, 177, 34, 158,
		89, 154, 205, 96, 55, 18, 138, 43, 96, 218, 215, 128, 124, 75, 138,
		243, 85, 25, 109, 117, 140, 26, 155, 249, 67, 167, 149, 231, 100, 6,
		41, 65, 214, 251, 232, 87, 72, 40, 182, 149, 154, 168, 31, 193, 126,
		215, 89, 28, 111, 219, 125, 182, 139, 235, 195, 197, 23, 234, 55, 58,
		63, 180, 68, 202, 206, 149, 75, 205, 248, 176, 67, 39, 178, 60, 98,
		193, 32, 238, 122, 96, 158, 222, 57, 183, 111, 210, 55, 188, 215,
		206, 180, 166, 150, 166, 106, 250, 55, 229, 72, 40, 69, 214, 216,
		104, 23, 40, 135, 212, 28, 127, 41, 80, 175, 174, 168, 115, 171, 197,
		89, 116, 92, 103, 246, 83, 216, 182, 176, 84, 37, 147, 35, 45, 219,
		172, 99, 226, 233, 73, 37, 124, 42, 72, 49, 242, 35, 127, 184, 134,
		117, 114, 135, 206}) != 0 {
		t.Error("A.1.3. Recipient key JSON encoding not spec compatible")
	}
	if encodeBase64URL(recipient1PrivateKeyJSON) != `OKOawDo13gRp2ojaHV7LFpZcgV7T6DVZKTyKOMTYUmKoTCVJRgckCL9kiMT03JGeipsEdY3mx_etLbbWSrFr05kLzcSr4qKAq7YN7e9jwQRb23nfa6c9d-StnImGyFDbSv04uVuxIp5Zms1gNxKKK2Da14B8S4rzVRltdYwam_lDp5XnZAYpQdb76FdIKLaVmqgfwX7XWRxv2322i-vDxRfqNzo_tETKzpVLzfiwQyeyPGLBIO56YJ7eObdv0je81860ppamavo35UgoRdbYaBcoh9QcfylQr66oc6vFWXRcZ_ZT2LawVCWTIy3brGPi6UklfCpIMfIjf7iGdXKHzg` {
		t.Error("A.1.3. Recipient key Base64URL encoding not spec compatible")
	}*/

	recipient1PrivateKey.algorithm = AlgorithmRSAOAEP
	recipient1PrivateKey.keyID = "first"

	// Initialization vector
	iv := []byte{227, 197, 117, 252, 2, 219, 233, 68, 180, 225, 77, 219}
	if encodeBase64URL(iv) != `48V1_ALb6US04U3b` {
		t.Fatal("A.1.4. Initialization vector Base64URL encoding not spec compatible")
	}

	// Create JWK version of our content encryption key
	jwcek, err := NewSymmetricKey(cek, AlgorithmA256GCM)
	if err != nil {
		t.Fatal(err)
	}

	// Create new JWE, encrypt it and add recipient
	jwe, err := New("", recipient1PrivateKey.Algorithm(), jwcek)
	if err != nil {
		t.Fatal(err)
	}
	if _, err := jwe.Write([]byte(msg)); err != nil {
		t.Fatal(err)
	}
	if err := jwe.AddRecipient(recipient1PrivateKey); err != nil {
		t.Fatal(err)
	}
	if err := jwe.Encrypt(nil, []byte{}, iv); err != nil {
		t.Fatal(err)
	}
	t.Logf(
		"First encryption\nJWE Algorithm: %s\nProtected Header Algorithm: %s\nProtected Header Encryption: %s\nCiphertext: %s\nAAD: %s\nTag: %s\nBuffer: %v\n",
		string(jwe.algorithm),
		jwe.GetProtectedHeaderValue(HeaderFieldAlgorithm),
		jwe.GetProtectedHeaderValue(HeaderFieldEncryption),
		jwe.ciphertext,
		jwe.additionalAuthenticatedData,
		jwe.tag,
		jwe.buffer.Bytes(),
	)

	// Spec test
	jweProtectedWanted := `eyJhbGciOiJSU0EtT0FFUCIsImVuYyI6IkEyNTZHQ00ifQ`
	if jwe.protected != jweProtectedWanted {
		t.Error("A.1.1. Protected header not spec compatible:")
		t.Logf("%s != %s\n", jwe.protected, jweProtectedWanted)
		for field, value := range jwe.getProtectedHeader() {
			t.Logf("\t%s:\t%s\n", string(field), value)
		}
	}

	// Spec test
	/*bAAD := []byte(jwe.AdditionalAuthenticatedData)
	if bytes.Compare(bAAD, []byte{
		101, 121, 74, 104, 98, 71, 99, 105, 79, 105, 74, 83, 85, 48, 69,
		116, 84, 48, 70, 70, 85, 67, 73, 115, 73, 109, 86, 117, 89, 121, 73,
		54, 73, 107, 69, 121, 78, 84, 90, 72, 81, 48, 48, 105, 102, 81}) != 0 {
		t.Error("A.1.5. Additional authenticated data Base64URL encoding not spec compatible")
	}*/

	// Spec test
	bCTWanted := []byte{229, 236, 166, 241, 53, 191, 115, 196, 174, 43, 73, 109, 39, 122,
		233, 96, 140, 206, 120, 52, 51, 237, 48, 11, 190, 219, 186, 80, 111,
		104, 50, 142, 47, 167, 59, 61, 181, 127, 196, 21, 40, 82, 242, 32,
		123, 143, 168, 226, 73, 216, 176, 144, 138, 247, 106, 60, 16, 205,
		160, 109, 64, 63, 192}
	bCT, err := decodeBase64URL(jwe.ciphertext)
	if err != nil {
		t.Fatal(err)
	}
	if bytes.Compare(bCT, bCTWanted) != 0 {
		t.Fatalf("A.1.6. Content encryption not spec compatible:\nWanted:\t%v\nGot:\t%v\n", bCTWanted, bCT)
	}

	/*compact, _ := jwe.Compact()
	if bytes.Compare(compact, []byte(`eyJhbGciOiJSU0EtT0FFUCIsImVuYyI6IkEyNTZHQ00ifQ.OKOawDo13gRp2ojaHV7LFpZcgV7T6DVZKTyKOMTYUmKoTCVJRgckCL9kiMT03JGeipsEdY3mx_etLbbWSrFr05kLzcSr4qKAq7YN7e9jwQRb23nfa6c9d-StnImGyFDbSv04uVuxIp5Zms1gNxKKK2Da14B8S4rzVRltdYwam_lDp5XnZAYpQdb76FdIKLaVmqgfwX7XWRxv2322i-vDxRfqNzo_tETKzpVLzfiwQyeyPGLBIO56YJ7eObdv0je81860ppamavo35UgoRdbYaBcoh9QcfylQr66oc6vFWXRcZ_ZT2LawVCWTIy3brGPi6UklfCpIMfIjf7iGdXKHzg.48V1_ALb6US04U3b.5eym8TW_c8SuK0ltJ3rpYIzOeDQz7TALvtu6UG9oMo4vpzs9tX_EFShS8iB7j6jiSdiwkIr3ajwQzaBtQD_A.XFBoMYUZodetZdvTiFvSkQ`)) != 0 {
		t.Error("A.1.7. JWE JSON compact serialization not spec compatible")
	}*/

	pk, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		t.Fatal(err)
	}
	recipient2PrivateKey, err := NewAsymmetricKey(pk, "second", AlgorithmRSAOAEP, KeyOperationEncrypt, KeyOperationDecrypt, KeyOperationWrapKey, KeyOperationUnwrapKey)
	if err != nil {
		t.Fatal(err)
	}

	if _, err := jwe.Decrypt(recipient1PrivateKey); err != nil {
		t.Fatal(err)
	}
	t.Logf(
		"First decryption\nJWE Algorithm: %s\nProtected Header Algorithm: %s\nProtected Header Encryption: %s\nCiphertext: %s\nAAD: %s\nTag: %s\nBuffer: %v\n",
		string(jwe.algorithm),
		jwe.GetProtectedHeaderValue(HeaderFieldAlgorithm),
		jwe.GetProtectedHeaderValue(HeaderFieldEncryption),
		jwe.ciphertext,
		jwe.additionalAuthenticatedData,
		jwe.tag,
		jwe.buffer.Bytes(),
	)
	jwe.RemoveRecipients()
	if err := jwe.AddRecipient(recipient2PrivateKey); err != nil {
		t.Fatal(err)
	}
	if err := jwe.Encrypt(nil, []byte{}, iv); err != nil {
		t.Fatal(err)
	}
	t.Logf(
		"Second encryption\nJWE Algorithm: %s\nProtected Header Algorithm: %s\nProtected Header Encryption: %s\nCiphertext: %s\nAAD: %s\nTag: %s\nBuffer: %v\n",
		string(jwe.algorithm),
		jwe.GetProtectedHeaderValue(HeaderFieldAlgorithm),
		jwe.GetProtectedHeaderValue(HeaderFieldEncryption),
		jwe.ciphertext,
		jwe.additionalAuthenticatedData,
		jwe.tag,
		jwe.buffer.Bytes(),
	)

	compact, err := jwe.Compact()
	if err != nil {
		t.Fatal(err)
	}
	jwe, err = compact.Expand()
	if err != nil {
		t.Fatal(err)
	}

	data, err := jwe.Decrypt(recipient2PrivateKey)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf(
		"Second decryption\nJWE Algorithm: %s\nProtected Header Algorithm: %s\nProtected Header Encryption: %s\nCiphertext: %s\nAAD: %s\nTag: %s\nBuffer: %v\n",
		string(jwe.algorithm),
		jwe.GetProtectedHeaderValue(HeaderFieldAlgorithm),
		jwe.GetProtectedHeaderValue(HeaderFieldEncryption),
		jwe.ciphertext,
		jwe.additionalAuthenticatedData,
		jwe.tag,
		jwe.buffer.Bytes(),
	)

	if bytes.Compare(data, []byte(msg)) != 0 {
		t.Fatalf("Decryption error\nWanted:\t%v\nGot:\t%v\n", []byte(msg), data)
	}
}

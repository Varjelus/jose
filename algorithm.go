package jose

type Algorithm string

// https://www.iana.org/assignments/jose/jose.xhtml
const (
	// alg
	AlgorithmHS256            Algorithm = "HS256"              // HMAC using SHA-256 | Required
	AlgorithmHS384            Algorithm = "HS384"              // HMAC using SHA-384 | Optional
	AlgorithmHS512            Algorithm = "HS512"              // HMAC using SHA-512 | Optional
	AlgorithmRS256            Algorithm = "RS256"              // RSASSA-PKCS1-v1_5 using SHA-256 | Recommended
	AlgorithmRS384            Algorithm = "RS384"              // RSASSA-PKCS1-v1_5 using SHA-384 | Optional
	AlgorithmRS512            Algorithm = "RS512"              // RSASSA-PKCS1-v1_5 using SHA-512 | Optional
	AlgorithmES256            Algorithm = "ES256"              // ECDSA using P-256 and SHA-256 | Recommended+
	AlgorithmES384            Algorithm = "ES384"              // ECDSA using P-256 and SHA-384 | Optional
	AlgorithmES512            Algorithm = "ES512"              // ECDSA using P-256 and SHA-512 | Optional
	AlgorithmPS256            Algorithm = "PS256"              // RSASSA-PSS using SHA-256 and MGF1 with SHA-256 | Optional
	AlgorithmPS384            Algorithm = "PS384"              // RSASSA-PSS using SHA-384 and MGF1 with SHA-384 | Optional
	AlgorithmPS512            Algorithm = "PS512"              // RSASSA-PSS using SHA-512 and MGF1 with SHA-512 | Optional
	Algorithmnone             Algorithm = "none"               // No digital signature or MAC performed | Optional
	AlgorithmRSA1_5           Algorithm = "RSA1_5"             // RSAES-PKCS1-v1_5 | Recommended-
	AlgorithmRSAOAEP          Algorithm = "RSA-OAEP"           // RSAES OAEP using default parameters | Recommended+
	AlgorithmRSAOAEP256       Algorithm = "RSA-OAEP-256"       // RSAES OAEP using SHA-256 and MGF1 with SHA-256 | Optional
	AlgorithmA128KW           Algorithm = "A128KW"             // AES Key Wrap using 128-bit key | Recommended
	AlgorithmA192KW           Algorithm = "A192KW"             // AES Key Wrap using 192-bit key | Optional
	AlgorithmA256KW           Algorithm = "A256KW"             // AES Key Wrap using 256-bit key | Recommended
	Algorithmdir              Algorithm = "dir"                // Direct use of a shared symmetric key | Recommended
	AlgorithmECDHES           Algorithm = "ECDH-ES"            // ECDH-ES using Concat KDF | Recommended+
	AlgorithmECDHESA128KW     Algorithm = "ECDH-ES+A128KW"     // ECDH-ES using Concat KDF and "A128KW" wrapping | Recommended
	AlgorithmECDHESA192KW     Algorithm = "ECDH-ES+A192KW"     // ECDH-ES using Concat KDF and "A192KW" wrapping | Optional
	AlgorithmECDHESA256KW     Algorithm = "ECDH-ES+A256KW"     // ECDH-ES using Concat KDF and "A256KW" wrapping | Recommended
	AlgorithmA128GCMKW        Algorithm = "A128GCMKW"          // Key wrapping with AES GCM using 128-bit key | Optional
	AlgorithmA192GCMKW        Algorithm = "A192GCMKW"          // Key wrapping with AES GCM using 192-bit key | Optional
	AlgorithmA256GCMKW        Algorithm = "A256GCMKW"          // Key wrapping with AES GCM using 256-bit key | Optional
	AlgorithmPBES2HS256A128KW Algorithm = "PBES2-HS256+A128KW" // PBES2 with HMAC SHA-256 and "A128KW" wrapping | Optional
	AlgorithmPBES2HS384A192KW Algorithm = "PBES2-HS384+A192KW" // PBES2 with HMAC SHA-384 and "A192KW" wrapping | Optional
	AlgorithmPBES2HS512A256KW Algorithm = "PBES2-HS512+A256KW" // PBES2 with HMAC SHA-512 and "A256KW" wrapping | Optional
	// enc
	AlgorithmA128CBCHS256 Algorithm = "A128CBC-HS256" // AES_128_CBC_HMAC_SHA_256 authenticated encryption algorithm | Required
	AlgorithmA192CBCHS384 Algorithm = "A192CBC-HS384" // AES_192_CBC_HMAC_SHA_384 authenticated encryption algorithm | Optional
	AlgorithmA256CBCHS512 Algorithm = "A256CBC-HS512" // AES_256_CBC_HMAC_SHA_512 authenticated encryption algorithm | Required
	AlgorithmA128GCM      Algorithm = "A128GCM"       // AES GCM using 128-bit key | Recommended
	AlgorithmA192GCM      Algorithm = "A192GCM"       // AES GCM using 192-bit key | Optional
	AlgorithmA256GCM      Algorithm = "A256GCM"       // AES GCM using 256-bit key | Recommended
	// alg
	AlgorithmEdDSA Algorithm = "EdDSA" // EdDSA signature algorithms | Optional
	// JWK
	AlgorithmRS1 Algorithm = "RS1" // RSASSA-PKCS1-v1_5 with SHA-1 | Prohibited
	// alg
	AlgorithmRSAOAEP384 Algorithm = "RSA-OAEP-384" // RSA-OAEP using SHA-384 and MGF1 with SHA-384 | Optional
	AlgorithmRSAOAEP512 Algorithm = "RSA-OAEP-512" // RSA-OAEP using SHA-512 and MGF1 with SHA-512 | Optional
	// JWK
	AlgorithmA128CBC Algorithm = "A128CBC" // AES CBC using 128 bit key | Prohibited
	AlgorithmA192CBC Algorithm = "A192CBC" // AES CBC using 192 bit key | Prohibited
	AlgorithmA256CBC Algorithm = "A256CBC" // AES CBC using 256 bit key | Prohibited
	AlgorithmA128CTR Algorithm = "A128CTR" // AES CTR using 128 bit key | Prohibited
	AlgorithmA192CTR Algorithm = "A192CTR" // AES CTR using 192 bit key | Prohibited
	AlgorithmA256CTR Algorithm = "A256CTR" // AES CTR using 256 bit key | Prohibited
	AlgorithmHS1     Algorithm = "HS1"     // HMAC using SHA-1 | Prohibited
)

package jose

type Compression string

const (
	CompressionDEFLATE Compression = "DEF" // DEFLATE (Specification Required)
)

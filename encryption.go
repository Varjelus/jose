package jose

import (
	"bytes"
	"encoding/json"
	"errors"
	"strings"
	"sync"
)

var (
	ErrNoRecipients      = errors.New("no recipients")
	ErrTooManyRecipients = errors.New("too many recipients")
	ErrAdditionalData    = errors.New("compact serialization may not be used with additional data")

	ErrInvalidKey  = errors.New("invalid key")
	ErrEncrypted   = errors.New("encrypted")
	ErrUnencrypted = errors.New("unencrypted")
	ErrMalformed   = errors.New("malformed")
)

type Recipient struct {
	header       map[HeaderField]string `json:"header,omitempty"`
	encryptedKey string                 `json:"encrypted_key,omitempty"`
}

func (rcpt *Recipient) GetHeaderValue(hf HeaderField) string {
	return rcpt.header[hf]
}

func (rcpt *Recipient) EncryptedKey() ([]byte, error) {
	return decodeBase64URL(rcpt.encryptedKey)
}

type Encryption struct {
	joseHeader

	// Auto-flatten on marshal
	*Recipient

	protected   string `json:"protected,omitempty"`
	unprotected string `json:"unprotected,omitempty"`

	initializationVector        string `json:"iv,omitempty"`
	additionalAuthenticatedData string `json:"aad,omitempty"`
	ciphertext                  string `json:"ciphertext"`
	tag                         string `json:"tag,omitempty"`

	recipients []*Recipient `json:"recipients,omitempty"`

	mutex  *sync.Mutex   `json:"-"`
	cek    *SymmetricKey `json:"-"`
	buffer *bytes.Buffer `json:"-"`
}

// New initializes a new Encryption without encryption.
func New(contentType string, alg Algorithm, skey *SymmetricKey) (*Encryption, error) {
	jwe := &Encryption{}

	if skey == nil {
		return nil, ErrNotSymmetric
	}

	// Set algorithms.
	if alg == "" {
		alg = Algorithmdir
	}
	switch alg { // RFC 7518 Section 4.1.
	case AlgorithmRSA1_5:
	case AlgorithmRSAOAEP:
	case AlgorithmRSAOAEP256:
	case AlgorithmA128KW:
	case AlgorithmA192KW:
	case AlgorithmA256KW:
	case Algorithmdir:
	case AlgorithmECDHES:
	case AlgorithmECDHESA128KW:
	case AlgorithmECDHESA192KW:
	case AlgorithmECDHESA256KW:
	case AlgorithmA128GCMKW:
	case AlgorithmA192GCMKW:
	case AlgorithmA256GCMKW:
	case AlgorithmPBES2HS256A128KW:
	case AlgorithmPBES2HS384A192KW:
	case AlgorithmPBES2HS512A256KW:
	default:
		return nil, ErrUnsupportedAlgorithm
	}
	jwe.algorithm = alg
	if err := jwe.setProtectedHeaderValue(HeaderFieldAlgorithm, string(alg)); err != nil {
		return nil, err
	}
	switch skey.algorithm { // RFC 7518 Section 5.1.
	case AlgorithmA128CBCHS256:
	case AlgorithmA192CBCHS384:
	case AlgorithmA256CBCHS512:
	case AlgorithmA128GCM:
	case AlgorithmA192GCM:
	case AlgorithmA256GCM:
	default:
		return nil, ErrUnsupportedAlgorithm
	}
	if err := jwe.setProtectedHeaderValue(HeaderFieldEncryption, string(skey.algorithm)); err != nil {
		return nil, err
	}

	jwe.mutex = new(sync.Mutex)
	jwe.buffer = new(bytes.Buffer)
	jwe.cek = skey
	jwe.setHeaderValue(HeaderFieldContentType, contentType)
	return jwe, nil
}

func (jwe *Encryption) getProtectedHeader() map[HeaderField]string {
	h, err := decodeBase64URL(jwe.protected)
	if err != nil {
		panic(err)
	}
	hm := make(map[HeaderField]string)
	if len(h) == 0 {
		return hm
	}
	if err := json.Unmarshal(h, &hm); err != nil {
		panic(err)
	}
	return hm
}

func (jwe *Encryption) getHeader() map[HeaderField]string {
	h, err := decodeBase64URL(jwe.unprotected)
	if err != nil {
		panic(err)
	}
	hm := make(map[HeaderField]string)
	if len(h) == 0 {
		return hm
	}
	if err := json.Unmarshal(h, &hm); err != nil {
		panic(err)
	}
	return hm
}

func (jwe *Encryption) GetProtectedHeaderValue(hf HeaderField) string {
	return jwe.getProtectedHeader()[hf]
}

func (jwe *Encryption) GetHeaderValue(hf HeaderField) string {
	return jwe.getHeader()[hf]
}

func (jwe *Encryption) setProtectedHeaderValue(hf HeaderField, val string) error {
	h := jwe.getProtectedHeader()
	h[hf] = val
	b, err := json.Marshal(h)
	jwe.protected = encodeBase64URL(b)
	return err
}

func (jwe *Encryption) setHeaderValue(hf HeaderField, val string) error {
	h := jwe.getHeader()
	h[hf] = val
	b, err := json.Marshal(h)
	jwe.unprotected = encodeBase64URL(b)
	return err
}

// Encrypt uses the provided symmetric key as a content encryption key.
// The second argument is to be used as additional authenticated data for AEAD.
// If the second argument is nil, AAD is generated automatically to integrity
// protect protected header members.
// If the second argument is an empty byte slice []byte{}, no AAD is used. This
// value MUST be passed in order to use JWE JSON compact serialization.
// If the second argument is non-nil and non-zero length, AAD will become
// encoded protected header joined with base64url encoded AAD parameter,
// per RFC 7516, Section 5.1, step 14.
// Calls to Encrypt are concurrency safe.
func (jwe *Encryption) Encrypt(data, aad, iv []byte) error {
	// 	jwe.mutex.Lock() // This function may read and write internals.
	// 	defer jwe.mutex.Unlock()

	// Even empty slice Write checks for encryption status.
	if _, err := jwe.Write(data); err != nil {
		return err
	}

	// AAD
	if aad == nil {
		println("nil")
		jwe.additionalAuthenticatedData = jwe.protected
	} else if len(aad) > 0 {
		println(len(aad))
		jwe.additionalAuthenticatedData = strings.Join([]string{
			jwe.protected,
			encodeBase64URL(aad),
		}, `.`)
	}

	// Encrypt data
	src := jwe.buffer.Bytes()
	b, err := jwe.cek.Encrypt(src, []byte(jwe.additionalAuthenticatedData), iv)
	if err != nil {
		return err
	}

	// Encode iv, ciphertext and authentication tag.
	jwe.initializationVector = encodeBase64URL(iv)
	jwe.ciphertext = encodeBase64URL(b[:len(src)])
	jwe.tag = encodeBase64URL(b[len(src):])

	jwe.cek = nil
	jwe.buffer.Reset()
	return nil
}

func (jwe *Encryption) Decrypt(key Key) ([]byte, error) {
	// 	jwe.mutex.Lock()
	// 	defer jwe.mutex.Unlock()

	if jwe.cek != nil {
		return nil, ErrUnencrypted
	}

	// Prepare buffer
	src, err := decodeBase64URL(jwe.ciphertext)
	if err != nil {
		return nil, err
	}

	// Decode initialization vector
	iv, err := decodeBase64URL(jwe.initializationVector)
	if err != nil {
		return nil, err
	}

	// Decode authentication tag
	tag, err := decodeBase64URL(jwe.tag)
	if err != nil {
		return nil, err
	}

	// Decrypt data
	switch key.(type) {
	case *SymmetricKey:
		b, err := key.(*SymmetricKey).Decrypt(src[:0], iv, append(src, tag...), []byte(jwe.additionalAuthenticatedData))
		if err != nil {
			return nil, err
		}
		if _, err = jwe.buffer.Write(b); err != nil {
			return jwe.buffer.Bytes(), err
		}
		jwe.ciphertext = ""
		jwe.tag = ""
		jwe.additionalAuthenticatedData = ""
		jwe.cek = key.(*SymmetricKey)
		return jwe.buffer.Bytes(), nil
	case *AsymmetricKey:
		recipient := jwe.GetRecipient(key.ID())
		if recipient == nil {
			if len(jwe.recipients) == 1 { // If there is only one recipient, try that key.
				recipient = jwe.recipients[0]
			} else {
				return nil, ErrInvalidKey
			}
		}
		cekb, err := recipient.EncryptedKey()
		if err != nil {
			return nil, err
		}
		if Algorithm(jwe.GetProtectedHeaderValue(HeaderFieldAlgorithm)) != Algorithmdir {
			cekb, err = key.(*AsymmetricKey).Decrypt(cekb, nil)
			if err != nil {
				return nil, err
			}
		}
		cek, err := NewSymmetricKey(cekb, Algorithm(jwe.GetProtectedHeaderValue(HeaderFieldEncryption)))
		return jwe.Decrypt(cek)
	default:
		return nil, ErrWrongKeyType
	}

	return nil, ErrNotSupported
}

func (jwe *Encryption) Multirecipient() bool {
	return len(jwe.recipients) > 1
}

func (jwe *Encryption) GetRecipient(keyid string) *Recipient {
	for _, recipient := range jwe.recipients {
		if recipient.GetHeaderValue(HeaderFieldKeyID) == keyid {
			return recipient
		}
	}
	return nil
}

func (jwe *Encryption) AddRecipient(public *AsymmetricKey) error {
	if jwe.cek == nil {
		return ErrEncrypted
	}

	wrappedKey, err := public.WrapKey(jwe.cek, nil)
	if err != nil {
		return err
	}

	// 	public.mutex.RLock()
	// 	shared.mutex.RLock()
	recipient := &Recipient{
		header: map[HeaderField]string{
			HeaderFieldAlgorithm:  string(public.algorithm),
			HeaderFieldEncryption: string(jwe.cek.algorithm),
			HeaderFieldKeyID:      public.ID(),
		},
		encryptedKey: encodeBase64URL(wrappedKey),
	}
	// 	public.mutex.RUnlock()
	// 	shared.mutex.RUnlock()

	// 	jwe.mutex.Lock() // This function may read and write internals.
	// 	defer jwe.mutex.Unlock()
	jwe.recipients = append(jwe.recipients, recipient)
	return nil
}

func (jwe *Encryption) RemoveRecipients() error {
	// 	jwe.mutex.Lock() // This function may read and write internals.
	// 	defer jwe.mutex.Unlock()

	jwe.recipients = []*Recipient{}

	return nil
}

func (jwe *Encryption) Compact() (CompactEncryption, error) {
	// 	jwe.mutex.Lock() // This function may read and write internals.
	// 	defer jwe.mutex.Unlock()

	if jwe.cek != nil {
		return nil, ErrUnencrypted
	}

	if jwe.Multirecipient() {
		return nil, ErrTooManyRecipients
	}
	if len(jwe.recipients) == 0 {
		return nil, ErrNoRecipients
	}
	// We have exactly one recipient

	// Compact JSON serialization may only be
	// used without additional authenticated data.
	if len(jwe.additionalAuthenticatedData) != 0 {
		return nil, ErrAdditionalData
	}

	return []byte(encodeBase64URL([]byte(strings.Join([]string{
		jwe.protected,
		jwe.recipients[0].encryptedKey,
		jwe.initializationVector,
		jwe.ciphertext,
		jwe.tag,
	}, `.`)))), nil
}

func (jwe *Encryption) Sign(*AsymmetricKey) (*Signature, error) {
	return nil, ErrNotSupported
}

func (jwe *Encryption) Read(p []byte) (n int, err error) {
	// 	jwe.mutex.Lock() // This function may read and write internals.
	// 	defer jwe.mutex.Unlock()

	if jwe.cek == nil {
		return 0, ErrEncrypted
	}
	return jwe.buffer.Read(p)
}

func (jwe *Encryption) Write(p []byte) (n int, err error) {
	// 	jwe.mutex.Lock() // This function may read and write internals.
	// 	defer jwe.mutex.Unlock()

	if jwe.cek == nil {
		return 0, ErrEncrypted
	}
	return jwe.buffer.Write(p)
}

type CompactEncryption []byte

func (cc *CompactEncryption) Expand() (*Encryption, error) {
	expanded, err := decodeBase64URL(string(*cc))
	if err != nil {
		return nil, err
	}
	s := strings.Split(string(expanded), `.`)
	if len(s) != 5 {
		return nil, ErrMalformed
	}

	// The protected header contents is to be copied to the recipient header
	p, err := decodeBase64URL(s[0]) // Decode protected header.
	if err != nil {
		return nil, err
	}
	header := make(map[HeaderField]string)
	if err := json.Unmarshal(p, &header); err != nil {
		return nil, err
	}

	alg, ok := header[HeaderFieldAlgorithm]
	if !ok {
		return nil, ErrMalformed
	}
	enc, ok := header[HeaderFieldEncryption]
	if !ok {
		return nil, ErrMalformed
	}
	cty, _ := header[HeaderFieldContentType]

	// Reconstruct
	ejwe := &Encryption{}
	ejwe.protected = s[0]
	ejwe.recipients = []*Recipient{&Recipient{
		header:       header,
		encryptedKey: s[1],
	}}
	ejwe.initializationVector = s[2]
	ejwe.ciphertext = s[3]
	ejwe.tag = s[4]

	ejwe.algorithm = Algorithm(alg)
	if err := ejwe.setProtectedHeaderValue(HeaderFieldAlgorithm, alg); err != nil {
		return nil, err
	}

	if err := ejwe.setProtectedHeaderValue(HeaderFieldEncryption, enc); err != nil {
		return nil, err
	}
	ejwe.mutex = new(sync.Mutex)
	ejwe.buffer = new(bytes.Buffer)
	ejwe.setHeaderValue(HeaderFieldContentType, cty)

	return ejwe, nil
}

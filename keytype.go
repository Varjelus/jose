package jose

type KeyType string

// https://www.iana.org/assignments/jose/jose.xhtml
var (
	KeyTypeEC  KeyType = "EC"  // Elliptic Curve; Recommended+
	KeyTypeRSA KeyType = "RSA" // RSA; Required
	KeyTypeOct KeyType = "oct" // Octet sequence; Required
	//KeyTypeOKP KeyType = "OKP" // Octet string key pairs; Optional
)

package jose

import (
	"errors"
)

var (
	ErrInvalidOperation = errors.New("invalid operation")
)

type KeyOperation string

const (
	KeyOperationSign       KeyOperation = "sign"       // Compute digital signature or MAC
	KeyOperationVerify     KeyOperation = "verify"     // Verify digital signature or MAC
	KeyOperationEncrypt    KeyOperation = "encrypt"    // Encrypt content
	KeyOperationDecrypt    KeyOperation = "decrypt"    // Decrypt content and validate decryption, if applicable
	KeyOperationWrapKey    KeyOperation = "wrapKey"    // Encrypt key
	KeyOperationUnwrapKey  KeyOperation = "unwrapKey"  // Decrypt key and validate decryption, if applicable
	KeyOperationDeriveKey  KeyOperation = "deriveKey"  // Derive key
	KeyOperationDeriveBits KeyOperation = "deriveBits" // Derive bits not to be used as a key
)

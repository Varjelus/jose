# jose

Go implementation of JOSE (JSON Object Signing and Encryption):
* JSON Web Algorithms (JWA) [RFC7518]
* JSON Web Signature (JWS) [RFC7515]
* JSON Web Encryption (JWE) [RFC7516]
* JSON Web Key (JWK) [RFC7517]
* JSON Web Key (JWK) Thumbprint [RFC7638]

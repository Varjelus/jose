package jose

type KeySet struct {
	Keys []Key `json:"keys"`
}

func (set *KeySet) GetKey(kid string) Key {
	for _, key := range set.Keys {
		if key.ID() == kid {
			return key
		}
	}
	return nil
}

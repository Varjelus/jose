package jose

import (
	"errors"
	"sync"
)

var (
	ErrUnsupportedAlgorithm = errors.New("unsupported algorithm")
	ErrWrongKeyType         = errors.New("wrong key type")
	ErrAlgorithmConflict    = errors.New("conflicting algorithms")
)

type Key interface {
	Allowed(KeyOperation) bool
	ID() string
	Algorithm() Algorithm
	Use() KeyUsage
	Operations() []KeyOperation
}

type anyKey struct {
	joseHeader

	keyType       KeyType        `json:"kty"`               // Key type (Section 4.1.); Required
	keyID         string         `json:"kid,omitempty"`     // Key ID (Section 4.5.); Unstructured; Case Sensitive; Optional
	use           KeyUsage       `json:"use,omitempty"`     // Public key use (Section 4.2.); Optional
	keyOperations []KeyOperation `json:"key_ops,omitempty"` // Key operations (Section 4.3.); Optional

	// Encryption
	saltInput      string `json:"p2s,omitempty"` // Salt Input (Appendix C.2.)
	iterationCount int    `json:"p2c,omitempty"` // Iteration count (Appendix C.2.)

	// Compression
	zip string `json:"zip,omitempty"` // Compression Algorithm (TODO)

	mutex *sync.RWMutex `json:"-"`
}

func (key *anyKey) Allowed(op KeyOperation) bool {
	for _, opi := range key.keyOperations {
		if opi == op {
			return true
		}
	}
	return false
}

func (key *anyKey) ID() string {
	return key.keyID
}

func (key *anyKey) Algorithm() Algorithm {
	return key.algorithm
}

func (key *anyKey) Use() KeyUsage {
	return key.use
}

func (key *anyKey) Operations() []KeyOperation {
	return key.keyOperations
}

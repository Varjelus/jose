package jose

const (
	ContentTypeJWK    string = "jwk+json"
	ContentTypeJWKSet        = "jwk-set+json"
)

package jose

type Curve string

const (
	CurveP256    Curve = "P-256"   // P-256 Curve | Recommended+
	CurveP384    Curve = "P-384"   // P-384 Curve | Optional
	CurveP512    Curve = "P-512"   // P-512 Curve | Optional
	CurveEd25519 Curve = "Ed25519" // Ed25519 signature algorithm key pairs | Optional
	CurveEd448   Curve = "Ed448"   // Ed448 signature algorithm key pairs | Optional
	CurveX25519  Curve = "X25519"  // X25519 function key pairs | Optional
	CurveX448    Curve = "X448"    // X448 function key pairs | Optional
)

package jose

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"errors"
	"math/big"
)

var (
	ErrNotSupported = errors.New("not implemented")

	ErrNotRSAKey     = errors.New("not RSA key")
	ErrNotECKey      = errors.New("not EC key")
	ErrNotAsymmetric = errors.New("not asymmetric key")

	ErrInvalidPrimes   = errors.New("invalid primes")
	ErrInvalidExponent = errors.New("invalid exponent")
)

type additionalPrime struct {
	r string `json:"r"`
	d string `json:"d"`
	t string `json:"t"`
}

type AsymmetricKey struct {
	anyKey

	// Public
	crv Curve `json:"crv,omitempty"` // Curve / Subtype of key pair [kty: EC/oct]

	// Private
	d string `json:"d,omitempty"` // Private key or exponent  [kty: EC/RSA]

	/* RSA specific fields */
	// Public
	n string `json:"n,omitempty"` // Modulus [kty: RSA]
	e string `json:"e,omitempty"` // Exponent [kty: RSA]
	// Private
	p    string            `json:"p,omitempty"`   // First Prime Factor [kty: RSA]
	q    string            `json:"p,omitempty"`   // Second Prime Factor [kty: RSA]
	dp   string            `json:"dp,omitempty"`  // First Factor CRT Exponent {D mod (P-1)} [kty: RSA]
	dq   string            `json:"dq,omitempty"`  // Second Factor CRT Exponent {D mod (Q-1)} [kty: RSA]
	qinv string            `json:"qi,omitempty"`  // First CRT Coefficient {Q^-1 mod P} [kty: RSA]
	oth  []additionalPrime `json:"oth,omitempty"` // Other primes info [kty: RSA]

	/* EC specific fields */
	// Public
	x string `json:"x,omitempty"` // X Coordinate [kty: EC]
	y string `json:"y,omitempty"` // Y Coordinate [kty: EC]

	private crypto.PrivateKey `json:"-"`
}

func NewAsymmetricKey(ki interface{}, kid string, alg Algorithm, operations ...KeyOperation) (*AsymmetricKey, error) {
	akey := &AsymmetricKey{}
	suitableOperations := []KeyOperation{}

	switch ki.(type) {
	case *rsa.PrivateKey:
		akey.keyType = KeyTypeRSA
		switch alg {
		case AlgorithmRSA1_5:
		case AlgorithmRSAOAEP:
		case Algorithm(""):
		default:
			return nil, ErrAlgorithmConflict
		}
		akey.algorithm = alg
		k := ki.(*rsa.PrivateKey)
		k.Precompute()
		akey.private = k
		akey.d = encodeBase64URLInt(k.D)
		akey.dp = encodeBase64URLInt(k.Precomputed.Dp)
		akey.dq = encodeBase64URLInt(k.Precomputed.Dq)
		akey.qinv = encodeBase64URLInt(k.Precomputed.Qinv)
		for i, prime := range k.Primes {
			if i == 0 {
				akey.p = encodeBase64URLInt(prime)
			} else if i == 1 {
				akey.q = encodeBase64URLInt(prime)
			} else {
				if len(k.Precomputed.CRTValues) < i-1 {
					return nil, ErrInvalidPrimes
				}
				akey.oth = append(akey.oth, additionalPrime{
					r: encodeBase64URLInt(k.Precomputed.CRTValues[i-2].R),
					d: encodeBase64URLInt(k.Precomputed.CRTValues[i-2].Exp),
					t: encodeBase64URLInt(k.Precomputed.CRTValues[i-2].Coeff),
				})
			}
		}
		suitableOperations = append(suitableOperations, []KeyOperation{
			KeyOperationDecrypt,
			KeyOperationSign,
			KeyOperationEncrypt,
			KeyOperationVerify,
			KeyOperationWrapKey,
			KeyOperationUnwrapKey,
		}...)
		akey.n = encodeBase64URLInt(k.N)
		akey.e = encodeBase64URLInt(big.NewInt(int64(k.E)))
	case *rsa.PublicKey:
		akey.keyType = KeyTypeRSA
		switch alg {
		case AlgorithmRSA1_5:
		case AlgorithmRSAOAEP:
		case Algorithm(""):
		default:
			return nil, ErrAlgorithmConflict
		}
		akey.algorithm = alg
		k := ki.(*rsa.PublicKey)
		akey.n = encodeBase64URLInt(k.N)
		akey.e = encodeBase64URLInt(big.NewInt(int64(k.E)))
		suitableOperations = append(suitableOperations, []KeyOperation{
			KeyOperationEncrypt,
			KeyOperationVerify,
			KeyOperationWrapKey,
		}...)
	case *ecdsa.PrivateKey:
		akey.keyType = KeyTypeEC
		return nil, ErrNotSupported
		k := ki.(*ecdsa.PrivateKey)
		akey.d = encodeBase64URLInt(k.D)
		akey.x = encodeBase64URLInt(k.X)
		akey.y = encodeBase64URLInt(k.Y)
		akey.crv = Curve(k.Params().Name)
		suitableOperations = append(suitableOperations, []KeyOperation{
			KeyOperationSign,
			KeyOperationVerify,
		}...)
	case *ecdsa.PublicKey:
		akey.keyType = KeyTypeEC
		return nil, ErrNotSupported
		k := ki.(*ecdsa.PublicKey)
		akey.x = encodeBase64URLInt(k.X)
		akey.y = encodeBase64URLInt(k.Y)
		akey.crv = Curve(k.Params().Name)
		suitableOperations = append(suitableOperations, []KeyOperation{
			KeyOperationVerify,
		}...)
	default:
		return nil, ErrNotSupported
	}

	// Check akey operation compability
	// TODO: What a mess
	var check bool
	for _, op := range operations {
		check = false
		for _, sop := range suitableOperations {
			check = true
			if op == sop { // Suitable operations
				if op == KeyOperationEncrypt || op == KeyOperationDecrypt || op == KeyOperationWrapKey || op == KeyOperationUnwrapKey {
					if akey.use == KeyUsageSignature {
						return nil, ErrInvalidOperation
					}
					akey.use = KeyUsageEncryption
				} else if op == KeyOperationSign || op == KeyOperationVerify {
					if akey.use == KeyUsageEncryption {
						return nil, ErrInvalidOperation
					}
					akey.use = KeyUsageSignature
				}
				akey.keyOperations = append(akey.keyOperations, op)
			}
		}
		if !check {
			return nil, ErrInvalidOperation
		}
	}
	akey.keyID = kid
	return akey, nil
}

func (akey *AsymmetricKey) Public() (crypto.PublicKey, error) {
	// // 	akey.mutex.RLock()
	// // 	defer akey.mutex.RUnlock()

	// Take a shortcut if we've already decoded private key.
	if akey.private != nil {
		switch akey.keyType {
		case KeyTypeEC:
			return akey.private.(*ecdsa.PrivateKey).Public(), nil
		case KeyTypeRSA:
			return akey.private.(*rsa.PrivateKey).Public(), nil
		default:
			return nil, ErrNotSupported
		}
	}

	switch akey.keyType {
	case KeyTypeEC:
		// Public parts
		x, err := decodeBase64URLInt(akey.x)
		if err != nil {
			return nil, err
		}
		y, err := decodeBase64URLInt(akey.y)
		if err != nil {
			return nil, err
		}
		// Select correct curve
		var c elliptic.Curve
		switch akey.crv {
		case CurveP256:
			c = elliptic.P256()
		case CurveP384:
			c = elliptic.P384()
		case CurveP512:
			c = elliptic.P521()
		default:
			return nil, ErrNotSupported
		}

		return &ecdsa.PublicKey{c, x, y}, nil
	case KeyTypeRSA:
		// Public parts
		if len(akey.n) == 0 || len(akey.e) == 0 {
			return nil, ErrNotAsymmetric
		}
		n, err := decodeBase64URLInt(akey.n)
		if err != nil {
			return nil, err
		}
		e, err := decodeBase64URLInt(akey.e)
		if err != nil {
			return nil, err
		}
		// Reduce exponent to int.
		var eInt64 int64
		if e.IsInt64() {
			eInt64 = e.Int64()
		} else {
			return nil, ErrInvalidExponent
		}

		return &rsa.PublicKey{
			N: n,
			E: int(eInt64),
		}, nil
	}
	return nil, ErrNotSupported
}

func (akey *AsymmetricKey) Private() (crypto.PrivateKey, error) {
	publicKey, err := akey.Public()
	if err != nil {
		return nil, err
	}

	// // 	akey.mutex.Lock()
	// // 	defer akey.mutex.Unlock()

	switch akey.keyType {
	case KeyTypeEC:
		// Private parts
		d, err := decodeBase64URLInt(akey.d)
		if err != nil {
			return nil, err
		}
		ecdsaPublic, ok := publicKey.(*ecdsa.PublicKey)
		if !ok {
			return nil, ErrNotECKey
		}
		akey.private = &ecdsa.PrivateKey{*ecdsaPublic, d}
		return akey.private, nil
	case KeyTypeRSA:
		// Private parts
		if len(akey.d) == 0 || len(akey.p) == 0 || len(akey.q) == 0 {
			return nil, ErrNotRSAKey
		}
		if len(akey.dp) == 0 || len(akey.dq) == 0 || len(akey.qinv) == 0 {
			return nil, ErrNotRSAKey
		}
		d, err := decodeBase64URLInt(akey.d)
		if err != nil {
			return nil, err
		}
		p, err := decodeBase64URLInt(akey.p)
		if err != nil {
			return nil, err
		}
		q, err := decodeBase64URLInt(akey.q)
		if err != nil {
			return nil, err
		}
		dp, err := decodeBase64URLInt(akey.dp)
		if err != nil {
			return nil, err
		}
		dq, err := decodeBase64URLInt(akey.dq)
		if err != nil {
			return nil, err
		}
		qinv, err := decodeBase64URLInt(akey.qinv)
		if err != nil {
			return nil, err
		}
		rsaPublic, ok := publicKey.(*rsa.PublicKey)
		if !ok {
			return nil, ErrNotRSAKey
		}
		pcv := rsa.PrecomputedValues{
			Dp:        dp,
			Dq:        dq,
			Qinv:      qinv,
			CRTValues: []rsa.CRTValue{}, // TODO: Precomputed Chinese remainder theorem values.
		}
		akey.private = &rsa.PrivateKey{*rsaPublic, d, []*big.Int{p, q}, pcv}
		return akey.private, nil
	default:
		return nil, ErrNotSupported
	}

	return nil, ErrNotSupported
}

func (akey *AsymmetricKey) WrapKey(skey *SymmetricKey, label []byte) ([]byte, error) {
	// Check permissions
	// // 	akey.mutex.RLock()
	if akey.use != KeyUsageEncryption {
		// // 		akey.mutex.RUnlock()
		return nil, ErrInvalidOperation
	}
	if !akey.Allowed(KeyOperationWrapKey) {
		// // 		akey.mutex.RUnlock()
		return nil, ErrInvalidOperation
	}
	// // 	akey.mutex.RUnlock()

	// // 	skey.mutex.RLock()
	skeyBytes, err := decodeBase64URL(skey.k)
	if err != nil {
		return nil, err
	}
	// // 	skey.mutex.RUnlock()

	return akey.Encrypt(skeyBytes, label)
}

func (akey *AsymmetricKey) Encrypt(src, label []byte) (b []byte, err error) {
	// Check permissions
	// // 	akey.mutex.RLock()
	if akey.use != KeyUsageEncryption {
		// // 		akey.mutex.RUnlock()
		return nil, ErrInvalidOperation
	}
	if !akey.Allowed(KeyOperationEncrypt) {
		// // 		akey.mutex.RUnlock()
		return nil, ErrInvalidOperation
	}
	// // 	akey.mutex.RUnlock()

	// Must be unlocked before calling Public method.
	publicKey, err := akey.Public()
	if err != nil {
		return nil, err
	}

	// // 	akey.mutex.RLock()
	// // 	defer akey.mutex.RUnlock()
	switch akey.keyType {
	case KeyTypeRSA:
		rsaPublicKey, ok := publicKey.(*rsa.PublicKey)
		if !ok {
			return nil, ErrNotRSAKey
		}
		switch akey.algorithm {
		case Algorithm(""):
			fallthrough
		case Algorithmnone:
			return src, nil
		case AlgorithmRSAOAEP:
			b, err := rsa.EncryptOAEP(sha256.New(), rand.Reader, rsaPublicKey, src, label)
			if err != nil {
				return nil, err
			}
			return b, nil
		default:
			return nil, ErrNotSupported
		}
	default:
		return nil, ErrNotSupported // TODO
	}

	return nil, ErrNotSupported
}

// Decrypt finds and decrypts the content encryption key and
// decrypts the JWE's ciphertext with that.
func (akey *AsymmetricKey) Decrypt(src, label []byte) ([]byte, error) {
	privateKey, err := akey.Private()
	if err != nil {
		return nil, err
	}

	switch akey.keyType {
	case KeyTypeRSA:
		switch akey.algorithm {
		case AlgorithmRSAOAEP:
			rsaPrivateKey, ok := privateKey.(*rsa.PrivateKey)
			if !ok {
				return nil, ErrWrongKeyType
			}
			return rsa.DecryptOAEP(sha256.New(), rand.Reader, rsaPrivateKey, src, label)
		case AlgorithmRSA1_5:
			rsaPrivateKey, ok := privateKey.(*rsa.PrivateKey)
			if !ok {
				return nil, ErrWrongKeyType
			}
			return rsa.DecryptPKCS1v15(rand.Reader, rsaPrivateKey, append(src, label...))
		case Algorithmnone:
		default:
			return nil, ErrUnsupportedAlgorithm
		}
	default:
		return nil, ErrNotSupported
	}
	return nil, ErrNotSupported
}

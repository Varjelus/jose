package jose

import (
	"crypto/aes"
	"crypto/cipher"
	"errors"
	"sync"
)

var (
	ErrNotSymmetric = errors.New("not asymmetric key")
	ErrKeyLength    = errors.New("wrong key length")
)

type SymmetricKey struct {
	anyKey

	k string `json:"k"` // Key Value [kty: oct]
}

func NewSymmetricKey(k []byte, alg Algorithm) (*SymmetricKey, error) {
	if alg == Algorithm("") {
		alg = Algorithmnone
	}

	skey := &SymmetricKey{}
	skey.keyType = KeyTypeOct
	skey.algorithm = alg
	skey.use = KeyUsageEncryption
	skey.keyOperations = []KeyOperation{
		KeyOperationEncrypt,
		KeyOperationDecrypt,
	}
	skey.mutex = new(sync.RWMutex)
	if len(k) > 0 {
		skey.k = encodeBase64URL(k)
	}
	if _, err := skey.Cipher(); err != nil {
		return nil, err
	}
	return skey, nil
}

// Cipher returns the cipher.Block interface of the key.
// Calls to Cipher are safe for concurrency.
func (skey *SymmetricKey) Cipher() (cipher.Block, error) {
	// 	skey.mutex.RLock() // This function may read internals.
	// 	defer skey.mutex.RUnlock()

	k, err := decodeBase64URL(skey.k)
	if len(k) == 0 {
		return nil, nil
	}

	var block cipher.Block
	switch skey.algorithm {
	case AlgorithmA128CBCHS256:
		fallthrough
	case AlgorithmA192CBCHS384:
		fallthrough
	case AlgorithmA256CBCHS512:
		return nil, ErrNotSupported // TODO
	case AlgorithmA128GCM:
		if len(k) != 16 {
			return nil, ErrKeyLength
		}
		block, err = aes.NewCipher(k)
		fallthrough
	case AlgorithmA192GCM:
		if len(k) != 16 {
			return nil, ErrKeyLength
		}
		block, err = aes.NewCipher(k)
		fallthrough
	case AlgorithmA256GCM:
		if block == nil && err == nil {
			if len(k) != 32 {
				return nil, ErrKeyLength
			}
			block, err = aes.NewCipher(k)
		}
		return block, err
	default:
		return nil, ErrNotSupported
	}
	return nil, ErrNotSupported
}

// Encrypt uses the symmetric key to encrypt src and additionalData.
// Calls to Encrypt are safe for concurrency.
func (skey *SymmetricKey) Encrypt(src, additionalData, iv []byte) ([]byte, error) {
	// 	skey.mutex.Lock() // This function may read and write internals.
	// 	defer skey.mutex.Unlock()

	// Check permissions
	if skey.use != KeyUsageEncryption {
		return nil, ErrInvalidOperation
	}
	if !skey.Allowed(KeyOperationEncrypt) {
		return nil, ErrInvalidOperation
	}

	switch skey.algorithm {
	case AlgorithmA128CBCHS256:
		fallthrough
	case AlgorithmA192CBCHS384:
		fallthrough
	case AlgorithmA256CBCHS512:
		return nil, ErrNotSupported // TODO
	case AlgorithmA128GCM:
		fallthrough
	case AlgorithmA192GCM:
		fallthrough
	case AlgorithmA256GCM:
		block, err := skey.Cipher()

		// Init crypto/cipher.AEAD
		ag, err := cipher.NewGCM(block)
		if err != nil {
			return nil, err
		}

		// Encrypt
		return ag.Seal(nil, iv, src, additionalData), nil
	default:
		return nil, ErrUnsupportedAlgorithm
	}

	return nil, ErrNotSupported
}

func (skey *SymmetricKey) Decrypt(dst, iv, src, aad []byte) ([]byte, error) {
	// 	skey.mutex.RLock() // This function may read internals.
	// 	defer skey.mutex.RUnlock()

	// Check permissions
	if skey.use != KeyUsageEncryption {
		return dst, ErrInvalidOperation
	}
	if !skey.Allowed(KeyOperationDecrypt) {
		return dst, ErrInvalidOperation
	}

	switch skey.algorithm {
	case AlgorithmA128CBCHS256:
		fallthrough
	case AlgorithmA192CBCHS384:
		fallthrough
	case AlgorithmA256CBCHS512:
		return nil, ErrNotSupported // TODO
	case AlgorithmA128GCM:
		fallthrough
	case AlgorithmA192GCM:
		fallthrough
	case AlgorithmA256GCM:
		block, err := skey.Cipher()
		if err != nil {
			return dst, err
		}
		ag, err := cipher.NewGCM(block)
		if err != nil {
			return dst, err
		}
		return ag.Open(dst, iv, src, aad)
	default:
		return dst, ErrUnsupportedAlgorithm
	}

	return dst, ErrInvalidKey
}

package jose

type joseHeader struct {
	algorithm          Algorithm `json:"alg,omitempty"` // Algorithm (RFC7515, Section 4.1.1 & RFC7516, Section 4.1.1 [Optional])
	x509Url            string    `json:"x5u,omitempty"` // URL of X.509 certificate or certificate chain URL (Section 4.6.); Optional
	x509Chain          []string  `json:"x5c,omitempty"`
	x509Thumbprint     string    `json:"x5t,omitempty"`      // A base64url-encoded SHA-1 thumbprint of the DER encoding of the X.509 certificate; Optional
	x509ThumbprintS256 string    `json:"x5t#S256,omitempty"` // A base64url-encoded SHA-256 thumbprint of the DER encoding of the X.509 certificate; Optional
}

type HeaderField string

const (
	HeaderFieldAlgorithm   HeaderField = "alg"
	HeaderFieldEncryption  HeaderField = "enc"
	HeaderFieldKeyID       HeaderField = "kid"
	HeaderFieldContentType HeaderField = "cty"
)

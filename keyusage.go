package jose

type KeyUsage string

const (
	KeyUsageSignature  KeyUsage = "sig" // Digital Signature or MAC
	KeyUsageEncryption KeyUsage = "enc" // Ecryption
)

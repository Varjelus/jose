package jose

import (
	"encoding/base64"
	"math/big"
)

func encodeBase64URL(b []byte) string {
	return base64.RawURLEncoding.EncodeToString(b)
}

func decodeBase64URL(s string) ([]byte, error) {
	return base64.RawURLEncoding.DecodeString(s)
}

func encodeBase64URLInt(z *big.Int) string {
	return base64.RawURLEncoding.EncodeToString(z.Bytes())
}

func decodeBase64URLInt(s string) (*big.Int, error) {
	b, err := base64.RawURLEncoding.DecodeString(s)
	if err != nil {
		return nil, err
	}
	return new(big.Int).SetBytes(b), nil
}

func encodeBase64URLString(s string) string {
	return base64.RawURLEncoding.EncodeToString([]byte(s))
}
